import React from 'react';
import {connect} from "react-redux";
import {
    Button,
    Grid, Image,
    Menu,
    Segment
} from "semantic-ui-react";
import {NavLink, Switch, useRouteMatch} from "react-router-dom";
import UserOnline from "../Auth/UserOnline";
import EditProfile from "./EditProfile";
import SetDevicePassword from "./SetDevicePassword";
import NotificationsSettings from "./NotificationsSettings";
import PrivacySettings from "./PrivacySettings";
import RecoverPlaylists from "./RecoverPlaylists";
import Apps from "./Apps";
import AccountOverview from "./AccountOverview";
import {signOut} from "../../Redux/actions/user";
import userPhoto from "../../assets/img/unnamed.png";
import styled from "styled-components";

function Profile({ user, signOut }) {

    let match = useRouteMatch();

    return (
        <Grid className="app-profile">
            <Grid.Column width={4} className="p-0">
                <AppProfileMenu fluid vertical inverted className="rounded-0">
                    <Menu.Item header className="text-center">
                        <Image avatar src={userPhoto} size="tiny"/>
                    </Menu.Item>
                    <Menu.Item icon="home" name='Account overview' exact as={NavLink} to={`${match.url}`}/>
                    <Menu.Item icon="pencil" name='Edit profile' exact as={NavLink} to={`${match.url}/edit`}/>
                    <Menu.Item icon="lock" name='Set device password' exact as={NavLink} to={`${match.url}/set-device-password`}/>
                    <Menu.Item icon="bell" name='Notification settings' exact as={NavLink} to={`${match.url}/notifications`}/>
                    <Menu.Item icon="lock" name='Privacy settings' exact as={NavLink} to={`${match.url}/privacy`}/>
                    <Menu.Item icon="redo" name='Recover playlists' exact as={NavLink} to={`${match.url}/recover-playlists`}/>
                    <Menu.Item icon="puzzle" name='Apps' exact as={NavLink} to={`${match.url}/apps`}/>
                    <Menu.Item
                        icon="sign-out"
                        className="text-left w-100 bg-danger"
                        content="Sign Out"
                        as={Button}
                        onClick={() => signOut()}
                    />
                </AppProfileMenu>
            </Grid.Column>

            <Grid.Column stretched width={12} className="p-0">
                <Segment className="rounded-0">
                    <Switch>
                        <UserOnline exact path="/profile" component={AccountOverview} />
                        <UserOnline exact path="/profile/edit" component={EditProfile} />
                        <UserOnline exact path="/profile/set-device-password" component={SetDevicePassword} />
                        <UserOnline exact path="/profile/notifications" component={NotificationsSettings} />
                        <UserOnline exact path="/profile/privacy" component={PrivacySettings} />
                        <UserOnline exact path="/profile/recover-playlists" component={RecoverPlaylists} />
                        <UserOnline exact path="/profile/apps" component={Apps} />
                    </Switch>
                </Segment>
            </Grid.Column>
        </Grid>
    );
}

const AppProfileMenu = styled(Menu).attrs({
    className: "app-profile-menu"
})`
    &.app-profile-menu {
        @media (max-width: 768px) {
            
        }
    }
`;

function mapStateToProps(state) {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps, {signOut})(Profile);