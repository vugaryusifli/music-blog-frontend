import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import {NavLink} from "react-router-dom";
import {
    Button,
    Container,
    Dropdown,
    Icon,
    Menu,
    Segment
} from "semantic-ui-react";
import styled from "styled-components";

function Header({ user }) {

    const [open, setOpen] = useState(false);
    const [openDropdown, setOpenDropdown] = useState(false);
    const [width, setWidth] = useState(window.innerWidth);

    function handleToggle() {
        setOpen(!open);
    }

    function handleOpenDropdown() {
        setOpenDropdown(!openDropdown);
    }

    function handleCloseDropdown() {
        setOpenDropdown(false);
    }

    function handleSideNavToggle() {
        setOpen(false);
    }

    useEffect(() => {
        function handleResize() {
            setWidth(window.innerWidth);
        }
        window.addEventListener("resize", handleResize);
        return () => window.removeEventListener("resize", handleResize);
    }, [width]);

    useEffect(() => {
        width > 768 && handleSideNavToggle();
        width > 768 && handleCloseDropdown();
        width < 768 && handleCloseDropdown();
    },[width]);

    useEffect(() => {
        if(open) {
            document.body.classList.add("scroll");
        } else {
            document.body.classList.remove("scroll");
        }
    }, [open]);

    return (
        <Segment inverted basic className="app-header">
            <Menu secondary inverted>
                <Container>
                    <Menu.Item header className="pl-0">
                        Music Blog
                    </Menu.Item>
                    <Menu.Menu position="right" className="app-toggle">
                        <Menu.Item
                            as={Button}
                            icon={
                                open ?
                                    <Icon name="close"/>
                                    :
                                    <Icon name="sidebar"/>
                            } onClick={handleToggle}
                        />
                    </Menu.Menu>
                    <AppOverlay open={open} onClick={handleToggle}/>
                    <AppMenu
                        drop={openDropdown ? 1 : 0}
                        position="right"
                        open={open}
                        onClick={e => e.stopPropagation()}
                    >
                        {
                            user.id ?
                                <>
                                    <Menu.Item icon="home" className="ml-0" exact name="Home" as={NavLink} to="/"/>
                                    <Menu.Item icon="info circle" exact name="About" as={NavLink} to="/about"/>
                                    <Dropdown
                                        item
                                        text="Pages"
                                        onClick={handleOpenDropdown}
                                        open={openDropdown}
                                        pointing={open ? "left" : 'top right'}
                                        onClose={handleCloseDropdown}
                                        icon={open ? "box" : "angle down"}
                                    >
                                        <Dropdown.Menu>
                                            <Dropdown.Item exact as={NavLink} to="/pages/category">Category</Dropdown.Item>
                                            <Dropdown.Item exact as={NavLink} to="/pages/playlist">Playlist</Dropdown.Item>
                                            <Dropdown.Item exact as={NavLink} to="/pages/artist">Artist</Dropdown.Item>
                                            <Dropdown.Item exact as={NavLink} to="/pages/blog">Blog</Dropdown.Item>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                    <Menu.Item icon="phone" exact name="Contact" as={NavLink} to="/contact" />
                                    <Menu.Item icon="help circle" exact name="Help" as={NavLink} to="/help" className="mr-0"/>
                                    <Menu.Item icon="user" exact name="Profile" as={NavLink} to="/profile" className="mr-0"/>
                                </>
                                :
                                <>
                                    <Menu.Item exact name="Sign In" as={NavLink} to="/auth/login"/>
                                    <Menu.Item exact name="Sign Up" as={NavLink} to="/auth/register"/>
                                </>
                        }
                    </AppMenu>
                </Container>
            </Menu>
        </Segment>
    );
}

const AppMenu = styled(Menu.Menu).attrs({
    className: "app-menu-menu"
})`
    &.app-menu-menu {
        .right.item.pointing {
          margin-left: .35714286em !important;
        }
        .icon.angle {
          margin-left: .5rem !important;
          margin-right: 0 !important;
          transition: transform .3s !important;
          display: block !important;
          transform: ${props => props.drop === 1 ? "rotate(-180deg)" : "rotate(0)"} !important;
        }
        @media (max-width: 768px) {
            flex-direction: column;
            transition: left .5s;
            width: 50%;
            position: fixed;
            padding: 0;
            background-color: #1b1c1d;
            left: ${props => props.open ? "0" : "-80%"};
            top: 68px;
            bottom: 0;
            z-index: 999;
            .item {
                justify-content: flex-start !important;
                width: 100%;
                height: 50px;
            }
            .dropdown {
                width: 100% !important;
            }
        }
    }
`;

const AppOverlay = styled.div.attrs({
    className: "app-menu-overlay"
})`
    &.app-menu-overlay {
        opacity: 0;
        visibility: visible;
        transition: opacity .5s;
        @media (max-width: 768px) {
            opacity: ${props => props.open ? 1 : 0};
            visibility: ${props => props.open ? "visible" : "hidden"};
            position: fixed;
            top: 68px;
            right: 0;
            bottom: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(0,0,0,0.5);
            z-index: 99;
        }
    }
`;

function mapStateToProps(state) {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(Header);