import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import {signUp} from "../../Redux/actions/user";

import Register from "./Register";
import {bindActionCreators} from "redux";

function RegisterContainer({signUp, user, reset}) {

    const [state, setState] = useState({
        name: "",
        email: "",
        password: "",
        passwordConfirmation: "",
        terms: false,
        errorName: "",
        errorEmail: "",
        errorPassword: "",
        errorPasswordConfirmation: "",
        errorTerms: "",
        loading: false,
        showPassword: false,
        showPasswordConfirmation: false,
        inputPassword: React.createRef(),
        inputPasswordConfirmation: React.createRef()
    });

    function handleChangeState(val, key) {
        setState({
            ...state,
            [key]: val
        });
    }

    function handleSendData() {
        setState({
            ...state,
            loading: true
        });
        signUp(state);
    }

    function handleShowPassword(param) {
        setState({
            ...state,
            showPassword: param,
        });
        state.inputPassword.current.focus();
    }

    function handleShowPasswordConfirmation(param) {
        setState({
            ...state,
            showPasswordConfirmation: param,
        });
        state.inputPasswordConfirmation.current.focus();
    }

    useEffect(() => {
        if(user.status === "error") {
            setState(state => ({
                ...state,
                loading: false,
                errorName: user.errorName || null,
                errorEmail: user.errorEmail || null,
                errorPassword: user.errorPassword || null,
                errorPasswordConfirmation: user.errorPasswordConfirmation || null,
                errorTerms: user.errorTerms || null,
            }));
            reset();
        }
    }, [user, reset]);


    return (
        <Register
            state={state}
            handleChangeState={handleChangeState}
            handleSendData={handleSendData}
            handleShowPassword={handleShowPassword}
            handleShowPasswordConfirmation={handleShowPasswordConfirmation}
        />
    );
}

function mapStateToProps(state) {
    return {
        user: state.user
    }
}

function mapDispatchTopProps(dispatch) {
    return {
        reset: () => {dispatch({type: "SIGN_OUT"})},
        signUp: bindActionCreators(signUp, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchTopProps)(RegisterContainer);