import React from 'react';
import {
    Button,
    Form,
    Grid,
    Header,
    Icon,
    Input,
    Label,
    Message,
    Segment
} from "semantic-ui-react";
import {Link} from "react-router-dom";

function Login({state, handleChangeState, handleSendData, handleShowPassword}) {

    function handleChangeFieldsState(e, key) {
        handleChangeState(e, key)
    }

    return (
        <Grid className="app-auth" textAlign="center" verticalAlign="middle">
            <Grid.Column>
                <Header as='h2' color='black' textAlign='center'>
                    Sign-in to your account
                </Header>
                <Form size='large' loading={state.loading}>
                    <Segment stacked>
                        <Form.Field>
                            <Form.Input
                                fluid
                                icon='mail'
                                iconPosition='left'
                                placeholder='E-mail address'
                                error={!!state.errorEmail && state.showValidateMessage}
                                onChange={(e) => handleChangeFieldsState(e.target.value, "email")}
                            />
                            {state.showValidateMessage ? state.errorEmail ? <span className="text-danger">{state.errorEmail}</span> : null : null}
                        </Form.Field>
                        <Form.Field>
                            <Form.Input
                                className="app-password"
                                labelPosition='right'
                                error={!!state.errorPassword && state.showValidateMessage}
                            >
                                <Input
                                    placeholder='Password'
                                    iconPosition='left'
                                    icon="lock"
                                    type={state.show ? 'text' : 'password'}
                                    onChange={(e) => handleChangeFieldsState(e.target.value, "password")}
                                />
                                <Label>
                                    <Icon onClick={() => handleShowPassword(!state.show)}  className="mr-0" name={state.show ? 'eye slash' : 'eye'}/>
                                </Label>
                            </Form.Input>
                            {state.showValidateMessage ? state.errorPassword ? <span className="text-danger">{state.errorPassword}</span> : null : null}
                        </Form.Field>
                        <Form.Field>
                            <Form.Checkbox
                                inline
                                label='Remember me'
                            />
                        </Form.Field>

                        <Button
                            color='blue'
                            fluid
                            size='large'
                            onClick={handleSendData}
                            className="mb-1"
                        >
                            Sign In
                        </Button>
                        {state.showDataMessage ? <span className="text-danger">Not such user found.</span> : null}
                        <p className="mt-3">
                            <Link to="#">Forgot password?</Link>
                        </p>
                    </Segment>
                </Form>
                <Message>
                    You are not a member? <Link to="/auth/register">Sign Up</Link>
                </Message>
            </Grid.Column>
        </Grid>
    );
}

export default Login;