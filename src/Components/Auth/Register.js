import React from 'react';
import {
    Button,
    Form,
    Grid,
    Header, Icon, Input, Label,
    Message,
    Segment
} from "semantic-ui-react";
import {Link} from "react-router-dom";

function Register({state, handleChangeState, handleSendData, handleShowPassword, handleShowPasswordConfirmation}) {

    function handleChangeFieldsState(e, key) {
        handleChangeState(e, key);
    }

    return (
        <Grid className="app-auth" textAlign="center" verticalAlign="middle">
            <Grid.Column>
                <Header as='h2' color='black' textAlign='center'>
                    Sign-up
                </Header>
                <Form size='large' loading={state.loading}>
                    <Segment stacked>
                        <Form.Field>
                            <Form.Input
                                fluid
                                icon='user'
                                iconPosition='left'
                                placeholder='Name'
                                error={!!state.errorName}
                                onChange={(e) => handleChangeFieldsState(e.target.value, "name")}
                            />
                            {state.errorName ? <span className="text-danger">{state.errorName}</span> : null}
                        </Form.Field>
                        <Form.Field>
                            <Form.Input
                                fluid
                                icon='mail'
                                iconPosition='left'
                                placeholder='E-mail address'
                                error={!!state.errorEmail}
                                onChange={(e) => handleChangeFieldsState(e.target.value, "email")}
                            />
                            {state.errorEmail ? <span className="text-danger">{state.errorEmail}</span> : null}
                        </Form.Field>
                        <Form.Field>
                            <Form.Input
                                className="app-password"
                                labelPosition='right'
                                error={!!state.errorPassword}
                            >
                                <Input
                                    ref={state.inputPassword}
                                    placeholder='Password'
                                    iconPosition='left'
                                    icon="lock"
                                    type={state.showPassword ? 'text' : 'password'}
                                    onChange={(e) => handleChangeFieldsState(e.target.value, "password")}
                                />
                                <Label>
                                    <Icon onClick={() => handleShowPassword(!state.showPassword)} className="mr-0" name={state.showPassword ? 'eye slash' : 'eye'}/>
                                </Label>
                            </Form.Input>
                            {state.errorPassword ? <span className="text-danger">{state.errorPassword}</span> : null}
                        </Form.Field>
                        <Form.Field>
                            <Form.Input
                                className="app-password"
                                labelPosition='right'
                                error={!!state.errorPasswordConfirmation}
                            >
                                <Input
                                    ref={state.inputPasswordConfirmation}
                                    placeholder='Password confirmation'
                                    iconPosition='left'
                                    icon="lock"
                                    type={state.showPasswordConfirmation ? 'text' : 'password'}
                                    onChange={(e) => handleChangeFieldsState(e.target.value, "passwordConfirmation")}
                                />
                                <Label>
                                    <Icon onClick={() => handleShowPasswordConfirmation(!state.showPasswordConfirmation)} className="mr-0" name={state.showPasswordConfirmation ? 'eye slash' : 'eye'}/>
                                </Label>
                            </Form.Input>
                            {state.errorPasswordConfirmation ? <span className="text-danger">{state.errorPasswordConfirmation}</span> : null}
                        </Form.Field>
                        <Form.Field>
                            <Form.Checkbox
                                required
                                inline
                                label='I agree to the terms and conditions'
                                error={!!state.errorTerms}
                                onChange={() => handleChangeFieldsState(!state.terms, 'terms')}
                            />
                        </Form.Field>

                        <Button
                            color='blue'
                            fluid size='large'
                            onClick={handleSendData}
                        >
                            Sign Up
                        </Button>
                    </Segment>
                </Form>
                <Message>
                    You have an account? <Link to="/auth/login">Sign In</Link>
                </Message>
            </Grid.Column>
        </Grid>
    );
}

export default Register;