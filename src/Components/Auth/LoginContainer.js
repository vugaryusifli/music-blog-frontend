import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import {signIn} from "../../Redux/actions/user";
import Login from "./Login";
import {bindActionCreators} from "redux";

function LoginContainer({signIn, reset, user}) {

    const [state, setState] = useState({
        email: "",
        password: "",
        errorEmail: "",
        errorPassword: "",
        showValidateMessage: false,
        showDataMessage: false,
        show: false,
        loading: false
    });

    function handleChangeState(val, key) {
        setState({
            ...state,
            [key]: val
        });
    }

    function handleSendData() {
        setState({
            ...state,
            loading: true
        });
        signIn(state);
    }

    function handleShowPassword(param) {
        setState({
            ...state,
            show: param
        });
    }

    useEffect(() => {
        if(user.status === "error") {
            setState(state => ({
                ...state,
                loading: false,
                errorEmail: user.errorEmail,
                errorPassword: user.errorPassword,
                showValidateMessage: true,
                showDataMessage: false
            }));
            reset();
        }
    }, [
        user.status,
        user.errorEmail,
        user.errorPassword,
        reset
    ]);

    useEffect(() => {
        if(user.status === "not_found") {
            setState(state => ({
                ...state,
                loading: false,
                showValidateMessage: false,
                showDataMessage: true
            }));
            reset();
        }
    }, [user.status, reset]);

    return (
        <Login
            state={state}
            handleChangeState={handleChangeState}
            handleSendData={handleSendData}
            handleShowPassword={handleShowPassword}
        />
    );
}

function mapStateToProps(state) {
    return {
        user: state.user
    }
}

function mapDispatchTopProps(dispatch) {
    return {
        reset: () => {dispatch({type: "SIGN_OUT"})},
        signIn: bindActionCreators(signIn, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchTopProps)(LoginContainer);