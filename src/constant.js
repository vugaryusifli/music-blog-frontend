export const API = {
    post_register: `${process.env.REACT_APP_API_URL}auth/register`,
    post_login: `${process.env.REACT_APP_API_URL}auth/login`,
    post_logout: `${process.env.REACT_APP_API_URL}auth/logout`,
    get_profile: `${process.env.REACT_APP_API_URL}profile`,
    get_users: `${process.env.REACT_APP_API_URL}users`,
};