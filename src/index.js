import React from 'react';
import ReactDOM from 'react-dom';
// Styles
import App from './App';
import * as serviceWorker from './serviceWorker';
import {BrowserRouter, Route} from "react-router-dom";
import {Provider} from "react-redux";
import {applyMiddleware, createStore} from "redux";
import rootReducers from "./Redux/reducers/rootReducers";
import {composeWithDevTools} from "redux-devtools-extension";
import thunk from "redux-thunk";
import {dispatchUser} from "./Redux/actions/user";
import axios from "axios";
import {toast, ToastContainer} from "react-toastify";
// Styles
import './index.css';
import './assets/css/style.min.css';
import "./App.css";
import 'semantic-ui-css/semantic.min.css';
import "react-toastify/dist/ReactToastify.min.css";

axios.interceptors.response.use(
    response => responseHandler(response)
    , function (error) {
        // Do something with response error
        if (error.response.status === 404) {
            toast.error("Something went wrong. Please check your connection or try again!", {
                position: "bottom-left"
            });
        } else if(error.response.status === 500) {
            toast.error("Something went wrong. Please, check your connection or try again!", {
                position: "bottom-left"
            });
        }
        return Promise.reject(error.response);
    });

const responseHandler = (response) => {
    if (response.data && (response.data.status === "error" || response.data.status === "not_found")) { // error || not_found
        toast.error("Something went wrong. Please, check your connection or try again!", {
            position: "bottom-left"
        });
    }
    return response;
};

const store = createStore(
    rootReducers,
    composeWithDevTools(
        applyMiddleware(thunk)
    )
);

const userInfo = JSON.parse(localStorage.getItem("user-data"));

if (userInfo) {
    const generalUserData = {
        id: userInfo.id,
        name: userInfo.name,
        email: userInfo.email,
        token: userInfo.token
    };
    store.dispatch(dispatchUser(generalUserData));
}

ReactDOM.render(
    <BrowserRouter>
        <Provider store={store}>
            <Route component={App} />
            <ToastContainer />
        </Provider>
    </BrowserRouter>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
