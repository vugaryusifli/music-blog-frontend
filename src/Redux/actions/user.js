import axios from "axios";
import {API} from "../../constant";

export const signUp = (state) => async (dispatch) => {
    try {
        const payload = await axios.post(API.post_register,
            {
                name: state.name,
                email: state.email,
                password: state.password,
                password_confirmation: state.passwordConfirmation,
                terms: state.terms
            }).then(res => res.data);

        const success = payload.data && payload.data.user;
        const error = payload && payload.errors;

        if(payload.status === "ok") {
            const generalUserData = {
                id: success.id,
                name: success.name,
                email:success.email
            };
            localStorage.setItem("user-data", JSON.stringify(generalUserData));
            dispatch(dispatchUser(generalUserData));
        } else {
            const validate = {
                status: payload.status,
                errorName: error && error.name && error.name[0],
                errorEmail: error && error.email && error.email[0],
                errorPassword: error && error.password && error.password[0],
                errorPasswordConfirmation: error && error.password_confirmation && error.password_confirmation[0],
                errorTerms: error && error.terms && error.terms[0]
            };
            dispatch(dispatchUser(validate));
        }
    } catch(e) {
        console.log(e);
    }
};

export const signIn = (state) => async (dispatch) => {
    try {
        const payload = await axios.post(API.post_login,
            {
                email: state.email,
                password: state.password,
            }).then(res => res.data);

        const success = payload.data;
        const error = payload.errors;

        if(payload.status === "ok") {
            const generalUserData = {
                token: success.token,
                id: success.user.id,
                name: success.user.name,
                email: success.user.email
            };
            dispatch(dispatchUser(generalUserData));
            localStorage.setItem("user-data", JSON.stringify(generalUserData));
        } else {
            const validate = {
                status: payload.status,
                errorEmail: error && error.email && error.email[0],
                errorPassword: error && error.password && error.password[0]
            };
            dispatch(dispatchUser(validate));
        }
    } catch(e) {
        console.log(e);
    }
};

export const dispatchUser = (user) => ({
    type: "GET_USERS",
    user
});

export const signOut = (token) => (dispatch) => {
    // let keysToRemove = ["user-online", "check-role"];
    //
    // keysToRemove.forEach(key =>
    //     localStorage.removeItem(key)
    // );

    // localStorage.removeItem("user-data");
    // localStorage.removeItem("check-role");

    // try {
    //     const payload = axios.post(API.post_logout,
    //         {
    //             token: token
    //         }).then(res => res.data);
    //
    //     console.log(payload);
    //
    // } catch(e) {
    //     console.log(e);
    // }

    localStorage.clear();
    dispatch(dispatchSignOut());
};

const dispatchSignOut = () => ({
    type: "SIGN_OUT"
});