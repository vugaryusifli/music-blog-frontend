export default function user(state = {}, action = {}) {
    switch (action.type) {
        case "GET_USERS":
            return action.user;
        case "SIGN_OUT":
            return {};
        default:
            return state;
    }
}