import React, {lazy, Suspense} from 'react';
import {Route, Switch} from "react-router-dom";
// HOC
import UserOffline from "./Components/Auth/UserOffline";
import UserOnline from "./Components/Auth/UserOnline";

import Loading from "./Components/Loading/Loading";
import Header from "./Components/Header/Header";
import {Container} from "semantic-ui-react";
const Home = lazy(() => import("./Components/Home/Home"));
const Login = lazy(() => import("./Components/Auth/LoginContainer"));
const Register = lazy(() => import("./Components/Auth/RegisterContainer"));
const About = lazy(() => import("./Components/About/About"));
const Contact = lazy(() => import("./Components/Contact/Contact"));
const Profile = lazy(() => import("./Components/Profile/ProfileContainer"));
const Category = lazy(() => import("./Components/Pages/Category/Category"));
const Playlist = lazy(() => import("./Components/Pages/Playlist/Playlist"));
const Artist = lazy(() => import("./Components/Pages/Artist/Artist"));
const Blog = lazy(() => import("./Components/Pages/Blog/Blog"));
const News = lazy(() => import("./Components/News/News"));
const Help = lazy(() => import("./Components/Help/Help"));
const Error = lazy(() => import("./Components/Error/Error"));

function App(props) {

    return (
        <>
            <Header/>
            <Suspense fallback={<Loading/>}>
                <Container>
                    <Switch>
                        <UserOffline exact path="/auth/login" component={Login} />
                        <UserOffline exact path="/auth/register" component={Register} />

                        <UserOnline exact path="/" component={Home}/>
                        <UserOnline exact path="/about" component={About} />

                        <UserOnline exact path="/pages/category" component={Category} />
                        <UserOnline exact path="/pages/playlist" component={Playlist} />
                        <UserOnline exact path="/pages/artist" component={Artist} />
                        <UserOnline exact path="/pages/blog" component={Blog} />

                        <UserOnline exact path="/news" component={News} />
                        <UserOnline exact path="/contact" component={Contact} />
                        <UserOnline exact path="/help" component={Help} />

                        <UserOnline path="/profile">
                            <Profile/>
                        </UserOnline>

                        <Route exact path="/*" component={Error}/>
                    </Switch>
                </Container>
            </Suspense>
        </>
    );
}

export default App;
